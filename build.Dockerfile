FROM rust:1.58 as builder
RUN cargo search --limit 0 # trigger crates.io index update for docker caching

WORKDIR /build
ADD . /build

RUN make build